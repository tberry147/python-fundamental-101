## Introduction to OOP's

## O = Object, O = Oriented, P = Programming


## Class Function
# Class is a collection or group of method (function)

## Object Function
## These are like variables

## Polymorphism
## Encapsulation
## Inheritance
## Data attraction

"""""
## Class

class Employee:

    def list_of_employees_attr(self, first_name, last_name, salary, hire_date, employee_id, ):
        self.first_name = first_name
        self.last_name = last_name
        self.salary = salary
        self.hire_date = hire_date
        self.employee_id = employee_id
        return None

    def display_employees_attr(self):
        print(f"\tMy first_name is: {self.first_name}\tMy last name is: {self.last_name}\tMy salary is: {self.salary}\tDate of hire is: {self.hire_date}\tMy employee id is: {self.employee_id}")
        return None
    

def main():
    employee_1 = Employee()      ##Class
    employee_2 = Employee()
    employee_1.list_of_employees_attr(first_name = "Imran", last_name = "Abdullahi", salary = 1000000, hire_date = "08-01-2025", employee_id = "0801",) ##Objects
    employee_2.list_of_employees_attr(first_name = "Iman", last_name = "Abdullahi", salary = 1000000, hire_date = "07-01-2030", employee_id = "0701",)
    employee_1.display_employees_attr()
    employee_2.display_employees_attr()



if __name__ == "__main__":
    main()

"""""


buckets = ["ecs.terraform.cluster.terraform", "ecs.working.cluster.terraform",
"elasticbeanstalk-us-east-1-735972722491",
"hqr.common.database.module.kojitechs.tf",
"kojibello.com",
"kojitechs.aws.eks.with.terraform.tf",
"kojitechs.github.organization"]

class s3_object:

    def list_all_buckets(self, bucket_name):
        self.bucket_name = bucket_name #This is a variable
        for bucket in self.bucket_name:
            print(f"\t{bucket}")
        return None
    
    def list_all_objects(self, objects_in_bucket):
        self.objects_in_bucket = objects_in_bucket
        for objects in objects_in_bucket:
            print(objects)
        return None

### Using Append to create a bucket

    def create_bucket(self, create_bucket):
        self.create_bucket = create_bucket
        print(f"Creating bucket with name: {self.bucket_name}")
        self.bucket_name.append(create_bucket)
        print(self.bucket_name)
        return None

## Using pop to delete a bucket

    def delete_bucket(self, delete_bucket):
        self.delete_bucket = delete_bucket
        print(f"Deleting bucket with name: {self.delete_bucket}")
        if delete_bucket in self.bucket_name:
            self.bucket_name.remove(delete_bucket)
            print(self.bucket_name)
        else:
            print(f"oops.. Bucket doesn't exist with the name: {delete_bucket}")

def main():
    s3_objects = s3_object()
    s3_objects.list_all_buckets(buckets)
    s3_objects.create_bucket("imran")
    s3_objects.delete_bucket("kojitechs.github")

if __name__ == "__main__":
       main()
