# # Dictionary Operation they are data structure that is written in key: and value that are seperated by colon
my_dict = {"First_Name": "Ibrahim", "Last_Name": "Abdul", "programming_lang": "python"}

print(my_dict.keys())
print(my_dict.values())
print(my_dict.get("programming_lang"))

new_dict = {"Brand": "Toyota", "Country": "Nigeria", "Model": "Tacoma"}
new_dict1 = {'brand': 'Ford','model': 'Mustang', 'year': 1964, 'country': ['Nigeria','Cameroon', 'Ghana']}

print(new_dict.keys())
print(new_dict1.pop("brand"))
print(new_dict1.items())

number = [1,2,3,4,5]

for each in number:
    print(each)

people = {1: {'name': 'John', 'age': '27', 'sex': 'Male'}, 
          2: {'name': 'Marie', 'age': '22', 'sex': 'Female'},
          3: {'name': 'Luna', 'age': '24', 'sex': 'Female', 'married': 'No'}}

print(people.get(1).get('sex'))
print(people.get(2).get('age'))
sec_people = people.get(1)

print(sec_people.get("sex"))


# Tuple Operation is unmuteable can also be a 1, or "string",

from pprint import pprint
tuple = ()
list = []

item =  ("phone", "tv", "phone")
print(item[1])

#Count will list the occurrance of an element in a list
print(item.count("phone"))

#Index is to indicate the position of the element in a list
print(item.index("phone"))

fruit = ["pineapple"]
fruits = ["pawpaw", "mango", "pear"]

fruits.append(fruit)
print(fruits)

# PYTHON OPERATION WITH NUMBER DATA TYPE
# Integer Operation

import builtins
from pprint import pprint

a = 5
b = 5.0
c = 5 + 2j

print(type(a))
print(type(b))
print(type(c))

pprint(dir(builtins))

x = 10
name = "Ibrahim"
j = 25.0

# Isinstance to validate a data type
print(isinstance(x, int))
print(isinstance(name, str))
print(isinstance(name, dict))
print(isinstance(j, int))