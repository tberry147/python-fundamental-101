import getpass 
# ## Operators are the pillars of any language, can be defined as a symbol which is responsibile for a particular operation between 2 opperands. Example "+" is an operators, peform addition operrands. may be values or variables.

# ########  Type of Operators  ########


# ### Arithmetic Operators  ###

# # + Addition
# # - Subtraction
# # * Multiplication
# # / Division
# # % Modulo
# # // Floor division
# # ** Exponential

from atexit import register
import builtins
import numbers
print(dir(builtins))

print(range(100))
print(list((range(2)))) ## It will unpack with list.

## Print range of 10, list it and start from 2. (This is a start index)
print(list(range(2,99)))

## To skip numbers (3)
print(list(range(1,11,3)))

## To print just even number

print(list(range(2,100,2)))

numbers = list(range(100))
for number in numbers:
    if number % 2 == 0:
        print(f"even number: {number}")
    else:
        print(f"odd number: {number}")



# # ## Comparison Operators  ## It takes values as inputs, perform some operations print True or False

print (1 == 2)
print (2 < 4)


# # ## Membership Operators, Python will take an input and check if () what if have provided is in a group () and result will be in bool (True/False)

# # ## in , not in

fruits = ["apple", "mango", "pawpaw"]

if "apple" in fruits:
    print('yes')

if "dates" not in fruits:
    print("no")

registered_student = ["Koji", "Ibrahim", "Imran"]
students_name = input("What is your name?:")

if students_name in registered_student:
    print(f"{students_name} has registered")
else:
    print("Student is yet to register")

## Identity Operators  ## It takes value as input then performs operations on the value and return a bool (True/False)
# ==


x = "Ibrahim"
y = "Imran"

result = x == y

print(result)

## Import a getpass module and use it as the key instead of input.
password = "IloveGod"
user_pass = getpass.getpass("What is your password?: ")
if password == user_pass:
    print("login successful")
else:
    print("incorrect password")


## Logical Operators It combines multiple conditions and arrive it a decision if both conditions are met.
## and / or

a = 2
b = 5
c = 7

print (a > b or a > c)
print("True")

####################################################################
## FIZZBUZZ theory in python!! 
# user_input = int(input("Please enter number?: "))
#user_unput is a multiple of 3: 
#     "FIZZ"
#user_unput is a multiple of 5: 
#     "BUZZ"
#user_unput is a multiple of BOTH 3 and 5: 
#     "FIZZBUZZ"
# RETURN user input


user_input = int(input("Please enter number?:"))
if (user_input % 3 == 0) and (user_input % 5 == 0):
    print("FIZZBUZZ")
elif user_input  % 3 == 0:
    print("FIZZ")
elif user_input % 5 == 0:
    print("BUZZ")
else:
    print(user_input)
    