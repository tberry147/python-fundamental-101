##Importing a script

def addition():
    print("Hi I am in addition!")

def subtraction():
    print("Hi I am in subtraction!")

def multiplication():
    print("Hi I am in multiplication!")

def division():
    print("Hi I am in division!")

##Entry point (handler) it has to be below

def function_handler():
    addition()


## Input this in order to avoid being present int your import.
if __name__ == "__main__":
    function_handler()