# DATA TYPE CONVERSION We can convert one type of number into another. This is also known as coercion (In order to fit our data intergrity)
import builtins
from pprint import pprint
a = 1
b = 2
print(a+b)

# Converting interger to string
c = 47

print(type(str(c)))

# Converting a numeric string to an interger
d = "2234"
e = "Ayo"
print(type(int(d)))

# Converting an interger to float
f = 6
print(type(float(f)))

# Converting an interger to bool
g = 78
print(type(bool(g)))

j = "we"
print(type(list(j)))

# Using eval it's used to combine both float and numbers together.
first_number = eval(input("please enter first number:"))
second_number = eval(input("please enter second number:"))

result = first_number + second_number

print(f"This is the addition of {first_number} number and {second_number} = {result}")