def test():
    print("Hello World!")

#test()

def test_return():
    return("Hello World!")

#test_return()

def test_parametre(schoolname):
    print("You're welcome to", schoolname)

#test_parametre("ianditech")

##Recap on args and kwargs
# If your value is a list data type then you should use args

def welcome_email(*names):
    for each in names:
     print(f"Hello {each} welcome to ianditech")

#welcome_email("Imran", "Tiwa")

# If your value is a dictionary data type then you should use kwargs

def intro(**data):
         print(f"Data type of argument", type(data))

         for k,v in data.items():
            print(f"{k} is {v}")
            
intro(first_name="Imran", Middle_name="Tiwa")