import os
import time

path = r"C:\Users\totim\OneDrive\Documents\python-class\python-fundamental-101\Python\Modules\today.sh"

#Traditional way of reading file
# file = open(path)
# print(file.readlines())

# file.close()

#New way of reading file
# with open(path) as file:
#     object = file.readlines()
#     print(object)

#Using expression
# if os.path.exists(path):
#     if os.path.isfile(path):
#         with open(path) as file:
#             object = file.readlines()
#             print(object)
#     else:
#         print("Please provide a file instead")
# else:
#     print(f"{path} is invalid")


#How to write to a file
# with open(path, "w") as file:
#     for i in range(1, 10):
#         file.write(f"i love you {i}\n")
#         file.write(f"i love you")
#     time.sleep(2)

#How to append to a file
# with open(path, "a") as file:
#     for i in range(1, 10):
#         file.write(f"i love you {i}\n")
#         file.write(f"i love you")
#     time.sleep(2)
