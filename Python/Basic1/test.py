from unicodedata import name


sampleDict = { "class": { "student": { "name": "Mike", "marks": { "physics": 70, "history": 80 }}}}

print(sampleDict["class"]["student"]["name"])
print(sampleDict.get('class').get('student').get('marks').get('physics'))

print(sampleDict["class"]["student"]["marks"]["history"])
print(sampleDict.get('class').get('student').get('name'))


# How many characters are here?
str1 = "P@#yn26at^&i5ve"
print(len(str1))


# How to reverse the order
str1 = "P@#yn26at^&i5ve"
print(str1[::-1])