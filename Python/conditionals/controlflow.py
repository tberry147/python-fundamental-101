## (for_loop and while_loop)

## Iterating over a list
from re import X
import getpass

fruits = ["pawpaw", "mango", "apple", "orange"]
for fruit in fruits:
    print(fruit)

## Iterating over a dict
## It only prints out the key

user_data = {
  "name": "John",
  "age": 30,
  "married": True,
  "divorced": False,
  "children": ("Ann","Billy"), 
  "pets": None,
  "cars": [ {"model": "BMW 230", "mpg": 27.5}, {"model": "Ford Edge", "mpg": 24.1} ]
}

for each in user_data:
    print(each)

## To print out the data use ".items()"
for key,value in user_data.items():
    print(key,value)


## To iterate over a string

x = "I love python"
for each in x:
    print(each)

### To iterate with condition

numbers = list(range(10))

for number in numbers:
    print(number)


fruits = ["pawpaw", "mango", "apple", "orange"]
new = []

for fruit in fruits:
    if "apple" == fruit:
        new.append("apple")
print(new)

x = list(range(100))

for number in x:
    if number % 2 == 0:
        print(f"even number: {number}")


## for_loop with an else block
## Expect an input = [password]
## print["login successful"] 
## try 2 more time

i = list(range(4))
user_password = "Imran"
user_login = getpass.getpass("enter password: ")

for i in range(1,4):
    print("Attempt",i)
    if user_password  == user_login:
        print("login successful")
    else:
        print("unsuccessful")


x = "i love kojitechs where i'll python end to end"

for each in x:
    print(each)


products = [( "Product1", 10),("Product2", 14),("Product3", 189),("Product4", 20),("Product5", 30)]

new_price = [] 

for product,price in products:
    new_price.append(price)
print(max(new_price))


## Using list comprehension

product = [product [1] for product in products ]
print(product)