x = {'resources': [{'resource_type': 'servicerole',
                'servicerole': {'action_count': 0,
                                'common': {'account': 'Data Science Lab',
                                           'account_id': '447899900727',
                                           'account_status': 'DEFAULT',
                                           'cloud': 'AWS',
                                           'creation_timestamp': '2018-05-03 '
                                                                 '20:11:35',
                                           'discovered_timestamp': '2018-08-23 '
                                                                   '15:43:07',
                                           'modified_timestamp': '2020-09-22 '
                                                                 '16:46:47',
                                           'namespace_id': 'arn:aws:iam::447908706727:role/dsl_rads_Coduhhbhjjilder',
                                           'organization_service_id': 13,
                                           'resource_id': 'servicerole:13:AROAI57JLG3EBSXGM46WQ:',
                                           'resource_name': 'dsl_rads_CoujjnhhitBuilder',
                                           'resource_type': 'servicerole'},
                                'create_date': '2018-05-03 20:11:35',
                                'description': 'Allows cross account access to '
                                               'CodeCommit in DSL account from '
                                               'Builder account',
                                'inline_policies': 1,
                                'managed_policy_count': 0,
                                'max_duration': 7200,
                                'name': 'dsl_rads_CodeCommitBuilder',
                                'path': '/',
                                'policy': '{"Version": "2012-10-17", ' '"Statement": [{"Action": ''"sts:AssumeRole", "Effect": ''"Allow", "Principal": {"AWS": '
                                          '["arn:aws:sts::298990994:assumed-role/US-HIS_AWS278899094-Developer/jjones@mmm.com", '
                                          '"arn:aws:sts::294412549994:assumed-role/US-HIS_AWS2967998994-Developer/tyehle@mmm.com"]}}]}',
                                'policy_count': 1,
                                'role_id': 'AROAI57JXGM46WQ',
                                'service_count': 0,
                                'trusted_accounts': [298900094],
                                'wildcard_service_count': 0}},
               {'resource_type': 'servicerole',
                'servicerole': {'action_count': 0,
                                'common': {'account': 'Data Science Lab',
                                           'account_id': '44779000027',
                                           'account_status': 'DEFAULT',
                                           'cloud': 'AWS',
                                           'creation_timestamp': '2018-03-22 '
                                                                 '14:51:48',
                                           'discovered_timestamp': '2018-08-23 '
                                                                   '15:43:07',
                                           'modified_timestamp': '2021-12-13 '
                                                                 '17:18:34',
                                           'namespace_id': 'arn:aws:iam::44678877727:role/US-HIS_AWS44789078827_dsl_nlp_udm',
                                           'organization_service_id': 13,
                                           'resource_id': 'servicerole:13:AROAI5ISYVWC3RRGFD4VO:',
                                           'resource_name': 'US-HIS_AWS447908706727_dsl_nlp_udm',
                                           'resource_type': 'servicerole'},
                                'create_date': '2018-03-22 14:51:48',
                                'description': 'This role allows for switching '
                                               'from SEC OPS Non Privileged '
                                               'Access role.',
                                'inline_policies': 0,
                                'last_used_date': '2021-12-13 16:06:36',
                                'managed_policy_count': 9,
                                'max_duration': 7200,
                                'name': 'US-HIS_AWS4490988727_dsl_nlp_udm',
                                'path': '/',
                                'policy': '{"Version": "2012-10-17", '
                                          '"Statement": [{"Action": '
                                          '"sts:AssumeRole", "Effect": '
                                          '"Allow", "Principal": {"AWS": '
                                          '["arn:aws:sts::64798909998:assumed-role/US-HIS_AWS6479988g98_Non_Privileged_Access/jimmy@mmm.com", '
                                          '"arn:aws:sts::64647898y698:assumed-role/US-HIS_AWS6799065598_Non_Privileged_Access/jjones@mmm.com", '
                                          '"arn:aws:sts::6466567888998:assumed-role/US-HIS_AWS6457899098_Non_Privileged_Access/ajayawardhana@mmm.com", '
                                          '"arn:aws:sts::6468997yty98:assumed-role/US-HIS_AWS66888yyy98_Non_Privileged_Access/smorris3@mmm.com", '
                                          '"arn:aws:sts::64gvyuinff98:assumed-role/US-HIS_AWS6yuioomj98_Non_Privileged_Access/apseyed@mmm.com", '
                                          '"arn:aws:sts::646ftyubiin8:assumed-role/US-HIS_AWS64hgguuuf8_Non_Privileged_Access/jglover2@mmm.com", '
                                          '"arn:aws:sts::646byuubbb8:assumed-role/US-HIS_AWS646g8n9nm8_Non_Privileged_Access/mconnor@mmm.com"]}}]}',
                                'policy_count': 1,
                                'service_count': 0,
                                'trusted_accounts': ['6hjiiikjj598'],
                                'wildcard_service_count': 0}}],
 'scopes': [],
 'selected_resource_type': 'servicerole',
 'supported_types': ['servicerole']}

 # when u see a list what should come to u mind is itteration. (for_each)
for each_resource in x.get('resources'):
    print(f"Acount Name: {each_resource.get('servicerole')['common']['account']}\nAccount_id: {each_resource['servicerole']['common']['account_id']}\nThe role arn: {each_resource['servicerole']['common']['namespace_id']}")