# password, dbname, username, port, host/endpoint
from imaplib import Commands
from multiprocessing.sharedctypes import Value
import psycopg2
from pprint import pprint
import time
import os
import json 
from pathlib import Path

## 
# pprint(dir(cursor))

DB_USER = os.environ["DB_USER"]
DB_HOST = os.environ["DB_HOST"]
DB_NAME = os.environ["DB_NAME"]
DB_PASSWORD = os.environ["DB_PASSWORD"]


# try:
#     with psycopg2.connect(user=DB_USER, database=DB_NAME, password=DB_PASSWORD, host=DB_HOST) as conn:
#         cursor = conn.cursor()
#         print(conn.get_dsn_parameters(), "\n")
#         cursor.execute("SELECT version();")
#         record = cursor.fetchone()
#         print(f"You are connected to postgres version {record}")
# except Exception as err:
#     print(err)

# CREATE A DATABASE TABLES USING PYTHON

# try:
#     with psycopg2.connect(user=DB_USER, database=DB_NAME, password=DB_PASSWORD, host=DB_HOST) as conn:
#         cursor = conn.cursor()
#         print("creating table in database...")
#         time.sleep(4)
#         create_table = """CREATE TABLE ianditechs
#                         (ID INT PRIMARY KEY NOT NULL, 
#                         FIRST_NAME  TEXT NOT NULL ,
#                         LAST_NAME   TEXT NOT NULL,
#                         PRICE   REAL);"""
#         cursor.execute(create_table)
#         conn.commit()
#         print("Table created successfuly....")

# except Exception as err:
#     print(err)

## READ TABLE USING PYTHONG

# try:
#     with psycopg2.connect(user=DB_USER, database=DB_NAME, password=DB_PASSWORD, host=DB_HOST) as conn:
#         cursor = conn.cursor()
#         print("creating table in database...")
#         time.sleep(4)
#         commands ="SELECT * FROM ianditechs"
#         cursor.execute(commands)
#         print(cursor.fetchall())

# except Exception as err:
#     print(err)


## INSERTING DATABASE IN A TABLE


ianditechs = json.loads(Path("ianditechs.json").read_text())

# for names in ianditechs:
#     #print(names.values()) # printing in a list
#     #print(tuple(names.values())) #printing in tuple

# try:
#     with psycopg2.connect(user=DB_USER, database=DB_NAME, password=DB_PASSWORD, host=DB_HOST) as conn:
#         cursor = conn.cursor()
#         print("inserting data in our database...")
#         time.sleep(4)
#         insert_data = command = """INSERT INTO ianditechs VALUES (%s,%s,%s,%s);"""

#         for names in ianditechs:
#             cursor.execute(insert_data, tuple(names.values()))

#         conn.commit()
#         print("Data inserted successfuly....")

# except Exception as err:
#     print(err)

# READ FROM DATABASE
try:
    with psycopg2.connect(user=DB_USER, database=DB_NAME, password=DB_PASSWORD, host=DB_HOST) as conn:
        cursor = conn.cursor()
        commands ="SELECT * FROM ianditechs"
        cursor.execute(commands)
        for i in cursor.fetchall():
            print(i)
            
       
except Exception as err:
    print(err)