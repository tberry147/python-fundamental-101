import platform
import os

from pprint import pprint
from re import X
# LINUX   ===>    ls, pwd, mkdir, rmdir, touch, cat, grep, sed, chmod, chown, man, watch
# WINDOWS ===>  dir, cls, mkdir, cd, net, reboot, restart, notepad, new-files, calls, watch

# pprint(dir(platform))

# print(platform.system())

# print(platform.machine())

# print(platform.node())

# print(platform.version())

# if platform.system() == "Windows":
#     print(f"This is a windows os\n{os.system('cls')}")
# else:
#     print(f"This is not a windows os\n{os.system('clear')}")

# # use user inputs
# cmd = input("Please Enter your desired command: ")
# run_command = os.system(cmd)

# if platform.system() == "Windows":
#     print(f"This is {platform.system()} os\n{run_command}")
# else:
#     print(f"This is {platform.system()}  os\n{run_command}")


 #Declaring Variable in another way
# x = 23
# y = 35
# z = 45
# x,y,z = 23,35,45

# print(z)

#Declaring List in another way

# fruits = ["apple", "mango", "cucumber", "dates", "plum"]

# first, second, third, *fruit = fruits
# print(*fruit)

#Declaring Dict in another way

# student_info = {"Name": "Imran","Age": "9", "Nationalty": "Nigerian"}

# print(student_info.items())

# x = ([('Name', 'Imran'), ('Age', '9'), ('Nationalty', 'Nigerian')])

# for k,v in x:
#     print(k,v)

student_info = {"Name": "Imran","Age": "9", "Nationalty": "Nigerian"}

x = ([('Name', 'Imran'), ('Age', '9'), ('Nationalty', 'Nigerian')])
y = []
for k,v in x:
    y.append(v)
print(y)
