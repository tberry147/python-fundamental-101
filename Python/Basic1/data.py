# Python Data Types

#String Operations

import numbers
from pprint import pprint
from typing import Dict

name = "imran Abdul"

pprint(dir(name))
print(name.capitalize())

e = "how are you doing?"
print(e.title())

b = "hello world"
print(b.istitle())

c = "where are you doing?"
print(c.split())

d = "come to me?"
print(d.strip('me?'))

print(" ".join(d))

f = "I love you oooooodsjkfdsfjfpoooooodsjfdfsooooodmsdfiooooo?"
print(f.count('o'))

names = "kojitech bellohsy hdgge"
print (name.count("e"))

g = "I love you oooooodsjkfdsfjfpoooooodsjfdfsooooodmsdfio?"
print(g.index('o'))
print(g[-1])
print(g[42])
print(g[0:8])

print(list(range(10)))


# # List Operations is muteable

#Append is used to add element at the end of a list
fruits = ["orange"]
pprint(dir(fruits))

fruits.append("pawpaw")
fruits.append("apple")
print(fruits)

#Insert method can add element at a given position in the list
fruits = ["pawpaw", "banana", "dates"]
fruits.insert(3, "avocado")
print(fruits)

#Extend method can join element to a list
list1 = ["bello", "imran", "ayo"]
list2 = ["Shade"]
list1.extend(list2)
print(list1)

#Remove is the method is used to remove an element from a list
list = ["ayo", "imran", "bello"]
list.remove("bello")
print(list)

#Pop can remove an element from any position in the list
list3 = ["ayo", "imran", "bello"]
list3.pop()
print(list3)

#List slice
x = ["Dubai", "Nigeria", "Gabon"]
secondx = x[0]
print(secondx[0:2])

x = ["Dubai", "Nigeria", "Gabon"]
print(x[1])

#sort
y= [2, 4343, 34, 45, 323, 3443]
y.sort()
print(y)
print(max(y)) #max
print(min(y)) #min


