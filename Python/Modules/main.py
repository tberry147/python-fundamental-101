import os

from pprint import pprint

path = r"C:\Users\totim\OneDrive\Documents\scr"
# path1 = "C:\\Users\\totim\\OneDrive\\Documents\\scr\\folder2"

# print(os.mkdir(path))

# print(os.removedirs(path))

# Module are any file (library) that ends with .py extension the main use is for re-useability
# i.e pprint, getpass.getpass, OS

# Methods available within OS module.

# To creare a dir using python us os.mkdir
#print(os.mkdir("folder1"))
#print(os.mkdir(path))

# To remove a dir using python use os/removedirs
#print(os.removedirs("folder1"))

# Using seperator
#print(os.sep())

# Get pwd
#print(os.getcwd())

# Change dir
#print(os.chdir())

# Using list DIR
#print(os.listdir(path))

#for each in os.listdir(path):
#    print(each)

# To create multiple DIR (specify the dir up)
#print(os.makedirs(path))

# To remove multiple DIR 
#print(os.removedirs(path))

# To rename DIR
#os.rename(path, r"C:\Users\totim\OneDrive\Documents\scr\im")

# Manage environment variable (use to manage secret, first you need to export GIT_TOKEN)
#os.environ("GIT_TOKEN")




## os.path is one of the most important sub module within os module

# To get base name
#pprint(dir(os.path))
#print(os.path.basename(path))

# To get the DIR name
#print(os.path.dirname(path))

# Join 2 DIR path
#path1 = r"C:\Users\totim\OneDrive\Documents"
#path2 = "scr"
#print(os.path.join(path1,path2))

# To split a DIR path to get a list
#path3 = "arn:aws:sts::64798909998:assumed-role/US-HIS_AWS6479988g98_Non_Privileged_Access/jimmy@mmm.com"

#print(os.path.split(path3))
#print(len(os.path.split(path3))) ##Using len

# How to get the size of the DIR per bytes
#print(os.path.getsize(path))

# To verify if the path is valid (Returns a boolean)
#print(os.path.exists(path))

# To verify if the user input valid (Returns a boolean)
#path6= r"C:\Users\totim\OneDrive\Documents\python-class\python-fundamental-101\Python\conditionals\operators.py"
#print(os.path.isfile(path6))

# write a programme to tell us if the path exist it should print all the files that are inside
# user_input = r"C:\Users\totim\OneDrive\Documents\scr"

# if os.path.exists(user_input):
#     print(f"The path for {user_input} is a valid")
#     if os.path.isdir(user_input):
#         print(f"The given path: {user_input} is a directory ")
#         for each_dir in os.listdir(user_input):
#             print(each_dir)
#     else:
#         print(f"please enter directory path only!")  
# else:
#     print(f"Your path {user_input} doesn't exists!!")




# LINUX   ===>    ls, pwd, mkdir, rmdir, touch, cat, grep, sed, chmod, chown, man, watch
# WINDOWS ===>  dir, cls, mkdir, cd, net, reboot, restart, notepad, new-files, calls, watch

#os.system("cls")

# print(os.system("clear"))

# print(os.system("cls"))
