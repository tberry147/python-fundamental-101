## All programms are written in function,to keep our code DRY

## Instead of repeating the greeting program than once lets create function

# Define the function
def greetings_function():
    name = input("What is your name?: ")
    print(f"You're welcome {name}")
    print("Thank you for subcribing to ianditechs")

# # Calling the greeting functions
# greetings_function()



# Creating Calculator function

def addition_function():
    x = eval(input("Enter number: "))
    print(2 + x)



# addition_function()


from ast import Add


def subtraction_function():
    x = eval(input("Enter number: "))
    print(2 - x)


# subtraction_function()

#Calculate even numbers

def even_numbers():
    for x in range(1,100):
        if x % 2 == 0:
            print(x, "is an even number")

#even_numbers()

def old_numbers():
    for x in range(1,100):
        if x % 1 == 0:
            print(x, "is an odd number")

#old_numbers()

def check_number():
    for x in range(1,100):
        if x % 2 == 0:
            print(x, "is an even number")
        else:
            print("This is an odd number")

#check_number()


## PARAMETERS AND ARGUEMENT


## Parameters are variables defined within the parentesis

def old_numbers(n):
    for x in range(1,n):
        if x % 1 == 0:
            print(x, "is an odd number")

#old_numbers(100)


## Positional Arguement

def add_two_digits(x,y):
    print(x + y)

#This is a positional arguement
#add_two_digits(7,9)


## Providing key word arguement
def add_two_digits(x=9,y=10):
    print(x+y)

#add_two_digits()

def multiply(y: int)-> int:
    print(y * 2)

#multiply("11")

##                                           Types of Function


##Print Function
def fizz_buzz_1(user_input):
    if (user_input % 3 == 0) and (user_input % 5 == 0):
        print("FIZZBUZZ")
    elif user_input  % 3 == 0:
        print("FIZZ")
    elif user_input % 5 == 0:
        print("BUZZ")
    else:
        print(user_input)

#fizz_buzz(15)

#Defining a return function
#It returns a function without an output

def fizz_buzz(user_input):
    if (user_input % 3 == 0) and (user_input % 5 == 0):
        return ("FIZZBUZZ")
    elif user_input  % 3 == 0:
        return("FIZZ")
    elif user_input % 5 == 0:
        return("BUZZ")
    else:
        return(user_input)

#fizz_buzz(5)

def send_email(name):
    return f"Sending welcome email to {name} \nHello {name} you have insufficient balance"

# send_email("Baca")

# email = send_email("Baca")

def welcome():
    email = send_email("Baca")
    print(email)




##Args and Kwargs Function
#They are optional parametres between a function block

## Args Function (Arbituary Arguement)

def sum_numbers(*args):
    for number in args:
        print(sum(number))

number = (1,2,3,4,5)

#sum_numbers(number)


## Key word arbituary arguement

student = {"name": "Imran", "age": "9", "Nationality": "Nigeria"}

def phones(**kwargs):
    for k,v in kwargs.items():
        print(k,v)

#phones(Samsung = 1000, iphone = 700)


## Putting together all the parameter concept 

def print_data(codeName, *args, **kwargs):
    #working with simple parameter
    #print(f"I'm learning the {codeName}")

#print_data("Quran")

    for arg in args:
        print("I am",{arg})



name ={     "I'm learning python",
      "i AM agent",
      "i AM pilot",
      "I am kwargs ('first_name', 'Koji')"}

     
def print_me(codeName, *args, **kwargs):
# working with args",
    for arg in args:
       print("i AM",arg)

## working with kwargs",
   
    for keyword in kwargs.items():
        print('I am kwargs',keyword)

  