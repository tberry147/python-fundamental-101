
# ### Logical Operators

# # < less than
# # > greater than
# # == equal
# # != not equal
# # or
# # and 

# x = 5
# y = 4 
# if x > y:
#     print(f'{x} is greater than {y}')

# if y < x:
#     print(f"{y} is less than {x}")

# print (y < x)


# ## If python were to check just one condition there's no need using an else block
# if y > x:
#     print(f"{y} is less than {x}")
# else:
#     print(f"{y} is not greater than {x}")


# x = 20

# if x < 50:
#     print(f"{x} is small")
# else:
#     print(f"{x} is big")



# # if 
# # else
# # elif

# cup_filled_with = input("What is in the cup?:") ## .lower (convert to instance)

# if cup_filled_with == "water":
#     print(f"The cup is filled with water")
# elif cup_filled_with == "wine":
#     print(f"The cup is filled with wine")
# elif cup_filled_with == "juice":
#     print(f"The cup is filled with juice")
# else:
#     print(f"{cup_filled_with}")

    

# temperature = eval(input("What is the temperature outside?:"))

# if temperature > 80:
#     print(f"it's hot outside")
# elif 45 <= temperature <= 80: 
#     print (f"its cool")
# else: 
#       print(f"its cold please get blanket")


president = input("who is the next president of Nigeria?:")

if president == "Peter Obi":
    print(f"Peter Obi will be the next president of Nigeria")
elif president == "Tinubu":
    print(f"Tinubu will be the next president of Nigeria")
elif president == "Atiku":
    print(f"Atiku will be the next president of Nigeria")
else:
    print(f"The youth needs to come up with strong candidate")

