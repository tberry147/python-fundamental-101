# What is CSV? Comma, Seperated and Values

import csv, json
from email import header
print(dir(csv))

# path = r"C:\Users\totim\OneDrive\Documents\python-class\python-fundamental-101\Python\file\csv\main.csv"
# #How to read CSV file
# with open(path) as file:
#     data = csv.reader(file)
#     for d in data:
#         print(d)


#How to write a CSV file

# header = ['name', 'area', 'country_code2', 'country_code3']
# data = [
#     ['Albania', '28748', 'AL', 'ALB'],
#     ['Algeria', '2381741', 'DZ', 'DZA'],
#     ['American Samoa', '199', 'AS', 'ASM']
# ]

# with open("address.csv", "w") as file:
#     writer = csv.writer(file)
# #   To create the header
#     writer.writerow(header)
#     writer.writerows(data)

#How to convert to a Dictwriter


header = ['name', 'area', 'country_code2', 'country_code3']
data = r"C:\Users\totim\OneDrive\Documents\python-class\python-fundamental-101\Python\file\json\parameter.json"

with open(data) as file:
    object = json.load(file)


with open("imran.csv", "w") as file:
    write = csv.DictWriter(file,fieldnames=header)

    write.writeheader()
    write.writerows(object)
