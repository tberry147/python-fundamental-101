from datetime import datetime
from pprint import pprint

import time



tim = datetime.now()

print(tim.minute)
print(tim.year)
print(tim.second)
print(tim.month)

# Upper case give you the full year, lower case gives you half
print(tim.strftime("%Y-%m-%d %H:%m"))

print(tim.strftime("%H:%m"))


for i in range(1, 10):
    print(f"I love you {i}")
    time.sleep(2)
