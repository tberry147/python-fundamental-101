from distutils import extension
import os
import platform
# This is to get the root, directory and file (its gives us the path)
# path = r"C:\Users\totim\OneDrive\Documents\python-class\python-fundamental-101"

# for r,d,f in os.walk(path):
#     print(r,d,f)

# This is using another arguement (topdown=True) print out in an orderly manner.
#path = input("Enter path: ")
#path = r"C:\Users\totim\OneDrive\Documents\python-class\python-fundamental-101\Python"
#for r,d,f in os.walk(path,topdown=True):
    #print(r)

#To get it in a very neat way.
# for r,d,f in os.walk(path,topdown=True):
#     for file in f:
#         print(file)

# To convert to boolean
# for r,d,f in os.walk(path,topdown=True):
#     for file in f:
#         print(bool(file))

# To print out the path
# for r,d,f in os.walk(path,topdown=True):
#     if bool(f):
#         print(f"There is a file present in this {path}")

##To check if its empty or not
# l = []
# if len(l) !=0:
#     print(f"{l} is not empty")



#To check if a dir exist
#path = input("Enter path: ")


# if os.path.exists(path): 
#     if os.path.isdir(path): 
#         for r,d,f in os.walk(path,topdown=True):
#             if len(f) !=0:
#                 for each_file in f:
#                     print(os.path.join(r,each_file))
#     else:     
#         print(f"{path} is a file, Provide a valid directory")           
# else:
#     print(f"Sorry invalid path {path}")






























# To check a particular ext

path = r"C:\Users\totim\OneDrive\Documents"  #input("Enter path: ")
d_ext = input("Enter extension: ")

if os.path.exists(path): 
    if os.path.isdir(path): 
        for r,d,f in os.walk(path):
            if len(f) !=0:
                for each_file in f:
                    if each_file.endswith(d_ext):
                        print(os.path.join(r,each_file))
    else:
        print("Please provide a valid directory")
else:
    print("Path is invalid")