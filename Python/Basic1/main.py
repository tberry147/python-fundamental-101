
import builtins
from tkinter import Y

from zmq import HELLO_MSG

# Data Type
print(type("bello")) #string

print (type(["bello", "imran"])) # list

print (type(("bello", "imran"))) # Tuple (unmutable)

print (type({"First_Name:" "Imran", "Last_Name:" "Abdullahi", "Country:" "American"})) # Set (Doesn't acccept duplicate)

print (type(3)) # integers

print (type(3.0)) # Float

print (type(True)) # Bool

print (type({})) # Dictionary

print (type({"First_Name": "Imran", "Last_Name": "Abdullahi", "Country": "American"}))

print ({"First_Name": "Imran", "Last_Name": "Abdullahi", "Country": "American"})

x = "hello"
y = "world"
d = x + " " + y

print (d)