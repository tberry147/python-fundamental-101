import builtins
from pprint import pprint
pprint(dir(builtins))

name = "Imran"
where = "world"
fruits = ["mango", "pear", "dates", "avocado"]
prices = 100.0

print(type(prices))

print("Hello world! my son's name is {}".format(name))

print(f"Hello world! my son's name is {name}")

print(f"Hello {where}! my son's name is {name}")

print(f"Here are the list of fruits {fruits} they have in the market")

print("The total amount is {}".format(prices))

print(f"The total amount is {prices}")

print(f"Here are the lists of fruits {fruits} and the total amount is {prices}")