from asyncore import write
import csv, json
from email import header

header = ['Num','Name','Hire_Date','Salary','Sick_Days_Remaining']
data = [
    ['0','Graham Chapman','03/15/14','50000.0','10'],
    ['1','John Cleese','06/01/15','65000.0','8'],
    ['2','Eric Idle','05/12/14','45000.0','10'],
    ['3','Terry Jones','11/01/13','70000.0','3'],
    ['4','Terry Gilliam','08/12/14','48000.0','7'],
    ['5','Michael Palin','05/23/13','66000.0','8']
]

with open("exercise.csv", "w") as file:
    writer = csv.writer(file)
    writer.writerow(header)
    writer.writerows(data)
